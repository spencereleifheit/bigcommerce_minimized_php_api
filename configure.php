<?php

/*	
	-------------------------------------------------------

	Configuration file for BigCommerce PHP API

		https://bitbucket.org/spencereleifheit/bigcommerce_minimized_php_api

	Code revised and re-written by: 

			Spencer Edward Leifheit  

				Bitbucket Handle: sixtyeighteleven   
					(sorry we don't pay for no stinking bitbucket here)

				Company: SixtyEightEleven Studios LLC
				Website: www.sixtyeighteleven.com

		Developed for www.theproductsuperstore.com 
			and
				The Online Investment Group

		Licensed under the GNU Public License Agreement 

	-------------------------------------------------------
*/

/*	
	-------------------------------------------------------

	ABOUT THIS PROJECT

		************************************************************************
		
		This is an extension and rewrite of the original Bigcommerce PHP API 
			developed by saranyan that is located at
			https://github.com/bigcommerce/bigcommerce-api-php

		The original PHP API requires Composer which I found to be a royal pain 
			in the ass and I wanted it to work with a single small set of files

		In attempting to debug the original API I found that Bigcommerce is
			horrible at supporting their own APIs and their existing Dev
			people are clueless if they can even speak english without myself
			being curious as to whether they will try to sell me a slushy

		I decided to write my own version of the API after being banned support
			for reasons of calling their support pathetic and their API a
			'worthless pile of dog shit that I wouldn't feed either of my ex wives',
			honestly, well... perhaps my second I would.

		This project developed --WITH PRIDE-- on a Mac OS X Mountain Lion platform

	-------------------------------------------------------
*/

if(!isset($testFlag)) { $testFlag = 0; }

/*	
	-------------------------------------------------------

	CONFIGURATION INSTRUCTIONS

		Set the appropriate Cipher type in the line below

			For MAMP : RC4-SHA

			For LAMP and WAMP: rsa_rc4_128_sha

			*NOTE*** --->  WAMP Server does not install a working implementation
				of cURL. You will need to fix that before this API will
				work for you

			That should be it!!

	-------------------------------------------------------
*/
		define ("PROJECT_NAME" , 'BigCommerce PHP API Testing Suite');

		define ("VERSION_NUMBER" , '1');
		define ("LATEST_REVISION" , '.001');

	/* -------------- VERIFY PEER setting ----------------- */

		define ("MY_PEER" , false);

	/* -------------- USE THIS CIPHER FOR MAMP ----------------- */

		define ("MY_CIPHER" , 'RC4-SHA');

	/* -------------- USE THIS CIPHER FOR WAMP or LAMP --------- */
	
		//define "MY_CIPHER" , 'rsa_rc4_128_sha';


	/* -------------- Set your store prefences here, they are located in the Bigcommerce 
	Control Panel under - Users, then edit the Users settings             - --------- */

		define ("STORE_URL" , 'https://www.theproductsuperstore.com');
		define ("API_KEY" , '427de403ad8eedc8726cde696f9a8d28152a5ac1');
		define ("USER_NAME" , 'admin');


	if($testFlag == 1) {
		echo "Config File loaded <br/>";
	}
// --- endOfLine

