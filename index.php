<?php

require "configure.php";
    
    echo "<h3>BigCommerce API Error Log</h3>";

    echo "<h4>Step 1: Include Class File and set NameSpace</h4>";

    /* --------------------------- Require API Class and set NameSpace ------ */
    require_once 'bigcommerce.php';
    use Bigcommerce\Api\Client as Bigcommerce;

    echo "---step complete";
    echo "<hr>";

    echo "<h4> Step 2: Set Parameters: </h4>";
    echo "setting store parameters<br/>";
    echo "store url is: ".$store_url."<br/>";
    echo "username is: ".$user_name."<br/>";
    echo "api key: ".$api_key."<br/>";
    echo "<br/>";


    /* --------------------------- Set API Connection and Store Parameters ------ */

    Bigcommerce::configure(array(
        'store_url' => $store_url,
        'username' => $user_name,
        'api_key' => $api_key
    ));

    echo "---step complete";
    echo "<hr>";

    /* --------------------------- Call setCipher and verifyPeer functions ------ */
    echo "<h4>Step 3: setCipher and verifyPeer </h4>";
    Bigcommerce::setCipher('RC4-SHA');
    echo "out of setCipher() function<br/>";

    echo "ready to send to verifyPeer() function<br/>";
    Bigcommerce::verifyPeer(false);
    echo "out of verifyPeer() function";
    echo "<br/><br/>";
    echo "---step complete";
    echo "<hr>";

    /* --------------------------- Call setTime() function to test connection ------ */

    echo "<h4>Step 4: Test connection with getTime() </h4>";

    $ping = Bigcommerce::getTime();

    if ($ping) 
    {
    echo "We have the time!!! ";
    echo $ping->format('H:i:s');
    echo "<br/>";
    }

    else {
        echo "we no have time :( <br/>";
    }

    echo "<br/>---step complete";
    echo "<hr>";

    echo "<h4>Step 5: Show us a product </h4>";







     /* --------------------------- End of Unit Testing ------ */

    echo "<h3>end of line</h3>";

