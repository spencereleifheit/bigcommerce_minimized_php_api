<?php
echo "<html>";
echo "<body>";

	error_reporting(E_ALL);


/*	
	-------------------------------------------------------

	API Test file for BigCommerce PHP API

	Code written by: 

			Spencer Edward Leifheit  

				Bitbucket Handle: sixtyeighteleven   
					(sorry we don't pay for no stinking bitbucket here)

				Company: SixtyEightEleven Studios LLC
				Website: www.sixtyeighteleven.com

		Developed for www.theproductsuperstore.com 
			and
				The Online Investment Group

		Licensed under the GNU Public License Agreement 

	-------------------------------------------------------
*/
    
	$testFlag = 1;

    echo "<h3>BigCommerce API Testing Suite</h3>";
    echo "<hr>";

    echo "<h4>Step 1: Verify Settings and Connection</h4>";

    /* --------------------------- Load Configuration settings and API ------ */
    
    include "configure.php";

    require "bigcommerce.php";

    use Bigcommerce\Api\Client as uBigC;

    /* --------------------------- Call setTime() function to test connection ------ */

	/* Time to configure the Core API   ---- */
	    
	    uBigC::configure(array(
		    'store_url' => STORE_URL,
		    'username' => USER_NAME,
		    'api_key' => API_KEY
	    ));

	  if ($testFlag == 1) {
		echo "API has been configured with store settings <br/>";
		}


	/* ----------  Time to set the Cipher and VerifyPeer, nothing works if this fails -- */

		$didcipher = uBigC::setCipher(MY_CIPHER);

	if ($testFlag == 1 && $didcipher) {
		echo "Finished setCipher() function <br/>";
		}

	if(!$didcipher) { 
		echo "Problem with Cipher settings. Check the configure.php file";
		break;
		}

		$didpeer = uBigC::verifyPeer(MY_PEER);

	if ($testFlag == 1 && $didpeer) {
		echo "Finished verifyPeer() function <br/>";
		}

	if(!$didpeer) { 
		echo "Problem with Peer settings. Check the configure.php file <br/>";
		break;
		}

	if($testFlag == 1) {
		 echo "Configuration Settings have been verified <br/>";
	}

	if($testFlag == 1){

		$ping = uBigC::getTime();

		if ($ping) {
			echo "Connection Test Successful<br/>";
		}

		else {
			echo "Connection Test Failure <br/>";
		}

	}

	echo "-----<strong>Step 1 Complete</strong>";
	echo "<hr>";

	echo "<h4>Step 2: Get Products</h4>";

    	
    	$products = uBigC::getProducts();

        foreach($products as $product) {
            echo $product->name;
            echo $product->price;
            echo "<br/>";
        }


	echo "-----<strong>Step 2 Complete</strong>";
	echo "<hr>";



	echo "API Test Complete <br/><br/>";



	 /* --------------------------- End of Unit Testing ------ */

	echo "<h3>end of line</h3>";

echo "</body>";
echo "</html>";
